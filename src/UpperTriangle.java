import java.util.Scanner;

public class UpperTriangle {
    int [][] array=new int[2][2];
    int rows= array.length;
    int cols=array[0].length;
    int count=0;
    public void isSquare(){
        Scanner scanner= new Scanner(System.in);
        System.out.println("Enter the number of rows and columns:");
        int rows = scanner.nextInt();
        int cols = scanner.nextInt();

        if(rows!=cols){
            System.out.println("Matrix should be Square");
        }
        else{
            System.out.println("Upper triangular matrix");
            System.out.println("Enter the elements in the array:");

            for(int outerIterate=0;outerIterate<rows;outerIterate++) {
                for (int innerIterate = 0; innerIterate < cols; innerIterate++) {
                    array[outerIterate][innerIterate] = scanner.nextInt();
                }
            }
            boolean isUpperTriangle=true;
            for(int outerIterate=0;outerIterate<rows;outerIterate++){
                for (int innerIterate = 0; innerIterate < cols; innerIterate++){
                    if(outerIterate>innerIterate&&array[outerIterate][innerIterate]!=0){
                        isUpperTriangle=false;
                        break;
                        }
                    }

                }
            if (isUpperTriangle) {
                System.out.println("yes");
            } else {
                System.out.println("no");
            }
            }
        }
    }
